# 01-hello-world

## Requirements

- You need `wasm_exec.js`. Use this command: `cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .`
- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 01-hello-world

go 1.17
```

## main.go

```golang
package main

import (
	"fmt"
)

func main() {

	fmt.Println("[From Go]", "👋 Hello World 🌍")

}
```

## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the http server
node index.js
```

- Open the webpage in your browser
- Open the console (developer tools)


