# 03-call-go-function

## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 03-call-go-function

go 1.17
```

## main.go

```golang
package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {
	// get the parameters
  message := args[0].String()
  return "😁 Hello " + message
}


func main() {
	// Expose the Go function to the JavaScript host
	js.Global().Set("Hello", js.FuncOf(Hello))
	
	<-make(chan bool)
}
```

* The **`Hello`** function takes two parameters and returns an **`interface{}`** type. 
* The function will be called synchronously from Javascript. 
* The **first** parameter (**`this`**) refers to **JavaScript's global object**. 
* The **second** parameter is a slice of **`[]js.Value`** representing the arguments passed to the Javascript function call.




## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the http server
node index.js
```

- Open the webpage in your browser

## Add a Go "Hey" function

- In `main.go`, create a new `Hey` that returns `"👋 Hey" + message`
- Call it from the JavaScript code to change the content of a `h2` tag

